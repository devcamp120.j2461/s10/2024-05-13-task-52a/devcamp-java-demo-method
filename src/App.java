public class App {
    static int i;
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        //tính tổng 100 số tự nhiên 1-100
        App a = new App();
        i = 10;
        System.out.println(a.sumNumbersV1());
        System.out.println(App.sumNumbersV1());
        System.out.println(sumNumbersV1());

        int[] input1 = {1, 5, 9};
        System.out.println(a.sumNumbersV2(input1));

        int[] input2 = new int[]{3, 6, 8, 11};
        System.out.println(sumNumbersV2(input2));

        //hiển thị kết quả theo số chẵn, lẻ
        printHello(24);
        printHello(99);
    }


    /* tính tổng 100 số tự nhiên 1-100 */
    public static int sumNumbersV1() {
        int total = 0;
        for (int i=1; i<=100; i++) {
            total += i;
        }
        return total;
    }


    /* tỉnh tổng 1 mảng số nguyên */
    public static int sumNumbersV2(int[] input) {
        int total = 0;
        for (int i=0; i<input.length; i++) {
            total += input[i];
        }
        return total;
    }

    /* hiển thị kết quả theo số chẵn, lẻ */
    public static void printHello(int input) {
        if (input % 2 == 0) {
            System.out.println("Day la so chan");
        } else {
            System.out.println("Day la so le");
        }
    }
}
